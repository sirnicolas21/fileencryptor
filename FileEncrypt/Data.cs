﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.ComponentModel;
namespace FileEncrypt
{
    public class QueueData : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public string Status { get; set; }
        public string SetStatus
        {
            set
            {
                Status = value;
                OnPropertyChanged("Status");
            }
        }
        //Status = value; 
        //OnPropertyChanged("Status"); 
        public string Input { get; set; }
        public string Output {get; set;   }
        public string FullInput { get; set; }
        public string FullOutput { get; set; }
        public string CrypticName { get; set; }
        protected void OnPropertyChanged(string property)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(property));
            }
        }
    }
    public class LogData : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public string Status { get; set; }
        public string SetStatus
        {
            set
            {
                Status = value;
                OnPropertyChanged("Status");
            }
        }
        //Status = value; 
        //OnPropertyChanged("Status"); 
        public string Input { get; set; }
        public string Output { get; set; }
        public string FullInput { get; set; }
        public string FullOutput { get; set; }

        protected void OnPropertyChanged(string Status)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(Status));
            }
        }
    }
}
