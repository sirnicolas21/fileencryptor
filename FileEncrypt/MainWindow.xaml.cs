﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
//using System.Windows.Shapes;
using Microsoft.Win32;
using System.IO;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Security.Cryptography;
using System.Diagnostics;

namespace FileEncrypt
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    
    public partial class MainWindow : Window
    {
        
        public MainWindow()
        {
            InitializeComponent();
        }
        PerformanceCounter cpuCounter = new PerformanceCounter("Processor","% Processor Time","_Total");
        static string FileExtension = @".scrypt";
        int ProgramIsOpen;
        int JobsDone;
        string TotalProcUsage = string.Empty;
        string password = @"minions1";
        public ObservableCollection<QueueData> QueueCollection { get { return _QueueCollection; } }
        ObservableCollection<QueueData> _QueueCollection = new ObservableCollection<QueueData>();
        public ObservableCollection<LogData> LogCollection { get { return _LogCollection; } }
        ObservableCollection<LogData> _LogCollection = new ObservableCollection<LogData>();
        FileEncrypt.BackgroundWorkers Workers = new BackgroundWorkers();
        //FileEncrypt.Jobs Jobs = new Jobs();
        QueueData Queuedata = new QueueData();
        LogData Logdata = new LogData();
        private void ButtonIput_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            Nullable<bool> result = dlg.ShowDialog();
            // Process open file dialog box results 
            if (result == true)
            {
                // Open document 
                LabelInput.Content = dlg.FileName;
                LabelInput.ToolTip = dlg.FileName;
                string FolderPath = System.IO.Path.GetDirectoryName(LabelInput.Content.ToString()) + @"\";
                string FileName = System.IO.Path.GetFileName(LabelInput.Content.ToString());
                string cryptor = Crypt.EncryptStringAES(FileName, password);
                LabelCrypticFilename.Content = cryptor;
                LabelCrypticFilename.ToolTip = cryptor;
                LabelOutput.Content = FolderPath + /*FileName*/RandomString(8,true) + FileExtension;
                LabelOutput.ToolTip = LabelOutput.Content.ToString();
                //ButtonOutput.IsEnabled = true;
                if (!File.Exists(LabelOutput.Content.ToString()))
                {
                    ButtonCrypt.IsEnabled = true;
                }
                else
                {
                    MessageBox.Show("File Already Exists", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                //ButtonOutput.IsEnabled = false;
                ButtonCrypt.IsEnabled = false;
                LabelOutput.Content = @"*";
                LabelOutput.Content = @"*";
            }
        }
        /// <summary>
        /// Generates a random string with the given length
        /// </summary>
        /// <param name="size">Size of the string</param>
        /// <param name="lowerCase">If true, generate lowercase string</param>
        /// <returns>Random string</returns>
        private string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }
        private void ButtonCrypt_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //FileEncrypt.Crypt Crypt = new Crypt();
                //Crypt.FileEncrypt(LabelInput.Content.ToString(), LabelOutput.Content.ToString());
                _QueueCollection.Add(new QueueData
                {
                    SetStatus = "Encrypt",
                    FullInput = LabelInput.Content.ToString(),
                    FullOutput = LabelOutput.Content.ToString(),
                    Input = System.IO.Path.GetFileName(LabelInput.Content.ToString()),
                    Output = System.IO.Path.GetFileName(LabelOutput.Content.ToString()),
                    CrypticName = LabelCrypticFilename.Content.ToString()
                });
                LabelInput.Content = "No Input Selected";
                LabelOutput.Content = "No Output";
                LabelCrypticFilename.Content = "CrypticName";
                //ButtonOutput.IsEnabled = false;
                ButtonCrypt.IsEnabled = false;
            }
            catch (Exception ex)
            {
               // MessageBox.Show(ex.Message);
            }
        }

        private void ButtonDeCryptInput_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            Nullable<bool> result = dlg.ShowDialog();
            // Process open file dialog box results 
            if (result == true)
            {
                LabelDecrypt.Content = dlg.FileName;
                LabelDecrypt.ToolTip = dlg.FileName;
                ButtonDecrypt.IsEnabled = true;
            }
        }

        System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 5);
            dispatcherTimer.Start();
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            ProgramIsOpen += 5;
            LabelCpuTotal.Text = Math.Round(cpuCounter.NextValue(), 0).ToString() + "%"; ;
            LabelProgramOpen.Text = ProgramIsOpen.ToString();
            try
            {

                if (!Workers.worker1.IsBusy | !Workers.worker2.IsBusy && _QueueCollection.Count > 0)
                {
                    foreach (QueueData Que in _QueueCollection){
                        /*if (_QueueCollection[0].Status == "EnCrypted" | _QueueCollection[0].Status == "DeCrypted")
                        {
                            _LogCollection.Add(new LogData
                            {
                                SetStatus = _QueueCollection[0].Status,
                                FullInput = _QueueCollection[0].FullInput,
                                FullOutput = _QueueCollection[0].FullOutput,
                                Input = _QueueCollection[0].Input,
                                Output = _QueueCollection[0].Output
                            });
                            //_QueueCollection.RemoveAt(0);
                            break;
                        }*/
                        if (Que.Status == "Encrypt")
                        {
                            //FileEncrypt.Crypt Crypt = new Crypt();
                            //Crypt.FileEncrypt(Que.FullInput, Que.FullOutput);
                            DoWorkEventHandler docrypt = (user, z) =>
                                {
                                    if (Que.Status == "Encrypt")
                                    {
                                        Que.SetStatus = "Working";
                                        //Crypt.FileEncrypt(Que.FullInput, Que.FullOutput);
                                        try
                                        {
                                            UnicodeEncoding UE = new UnicodeEncoding();
                                            byte[] key = UE.GetBytes(password);
                                            string cryptFile = Que.FullOutput;
                                            FileStream fsCrypt = new FileStream(cryptFile, FileMode.Create);
                                            RijndaelManaged RMCrypto = new RijndaelManaged();
                                            CryptoStream cs = new CryptoStream(fsCrypt, RMCrypto.CreateEncryptor(key, key), CryptoStreamMode.Write);
                                            FileStream fsIn = new FileStream(Que.FullInput, FileMode.Open);
                                            int data;
                                            while ((data = fsIn.ReadByte()) != -1)
                                            {

                                                cs.WriteByte((byte)data);
                                                Que.SetStatus = (fsIn.Position * 100 / fsIn.Length).ToString() + "%";
                                            }
                                            fsIn.Close();
                                            cs.Close();
                                            fsCrypt.Close();
                                            JobsDone++;
                                            Que.SetStatus = "EnCrypted";
                                            //FileStream fsCrypt2 = new FileStream(cryptFile+".fn", FileMode.Create);
                                            StringBuilder sb = new StringBuilder();
                                            TextWriter tw = new StreamWriter(cryptFile + ".fn");
                                            tw.WriteLine(Que.CrypticName);
                                            tw.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                           // MessageBox.Show(ex.Message);
                                            Que.SetStatus = "Error";
                                        }
                                    }
                                };
                            if (!IsFileLocked(Que.FullInput))
                            {
                                if (Workers.worker1.IsBusy)
                                {
                                    Workers.worker2.DoWork += docrypt;
                                    Workers.worker2.RunWorkerAsync();
                                }
                                else
                                {
                                    Workers.worker1.DoWork += docrypt;
                                    Workers.worker1.RunWorkerAsync();
                                }
                                QueueBox.ScrollIntoView(QueueBox.Items[QueueBox.Items.Count - 1]);
                            }
                            /*else
                            {
                                Que.SetStatus = "Error: File Locked";
                            }*/
                        }
                        else if (Que.Status == "DeCrypt")
                        {
                            try
                            {
                                DoWorkEventHandler dodecrypt = (user, z) =>
                                 {
                                     if (Que.Status == "DeCrypt")
                                     {
                                         Que.SetStatus = "Working";
                                         UnicodeEncoding UE = new UnicodeEncoding();
                                         byte[] key = UE.GetBytes(password);
                                         FileStream fsCrypt = new FileStream(Que.FullInput, FileMode.Open);
                                         RijndaelManaged RMCrypto = new RijndaelManaged();
                                         CryptoStream cs = new CryptoStream(fsCrypt,
                                             RMCrypto.CreateDecryptor(key, key),
                                             CryptoStreamMode.Read);
                                         FileStream fsOut = new FileStream(Que.FullOutput, FileMode.Create);
                                         int data;
                                         //int i=0;
                                         while ((data = cs.ReadByte()) != -1)
                                         {
                                            // i++;
                                             fsOut.WriteByte((byte)data);
                                             Que.SetStatus = (fsCrypt.Position * 100 / fsCrypt.Length).ToString() + "%";
                                         }
                                         fsOut.Close();
                                         cs.Close();
                                         fsCrypt.Close();
                                         JobsDone++;
                                         Que.SetStatus = "DeCrypted";
                                     }
                                 };
                                if (!IsFileLocked(Que.FullInput))
                                {
                                    if (Workers.worker1.IsBusy)
                                    {
                                        Workers.worker2.DoWork += dodecrypt;
                                        Workers.worker2.RunWorkerAsync();
                                    }
                                    else
                                    {
                                        Workers.worker1.DoWork += dodecrypt;
                                        Workers.worker1.RunWorkerAsync();
                                    }
                                    QueueBox.ScrollIntoView(QueueBox.Items[QueueBox.Items.Count-1]);
                                }
                                /*else
                                {
                                    Que.SetStatus = "Error: File Locked";
                                }*/

                            }
                            catch (Exception ex)
                            {
                                //MessageBox.Show(ex.Message);
                                Que.SetStatus = ex.Message;
                            }
                        }
                        LabelJobsDone.Text = JobsDone.ToString();
                    }
                    }
                }
            catch (Exception ex)
            {
               // MessageBox.Show(ex.Message);
            }
        }
        protected virtual bool IsFileLocked(string file)
        {

            FileStream stream = null;
            try
            {
                stream = new FileStream(file, FileMode.Open);
                //stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }
        private void ButtonDecrypt_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string tempin = LabelDecrypt.Content.ToString();
                string tempout = tempin;
                string decodedoutput = string.Empty;
                 int removeindex = tempout.IndexOf(System.IO.Path.GetFileName(tempout));
                
                if (File.Exists(tempout + ".fn"))// checks for the file that holds the encrypted filename
                {
                    try
                    {
                        TextReader tr = new StreamReader(tempout + ".fn");
                        decodedoutput = tr.ReadLine();
                        decodedoutput = Crypt.DecryptStringAES(decodedoutput, password);
                        tempout = (removeindex < 0) ? tempout : tempout.Remove(removeindex, System.IO.Path.GetFileName(tempout).Length);
                        decodedoutput = tempout + decodedoutput;
                    }
                    catch(Exception ex)
                    {
                        decodedoutput = tempout;
                    }
                }
                else
                { // if no file was found just use the filename that the primary encrypted file has
                    decodedoutput = tempout;
                }
                string tempstatus = "DeCrypt";
                if (File.Exists(decodedoutput)) //checks if the file exists and drops a file error status
                    tempstatus = "File Error";
                _QueueCollection.Add(new QueueData
                { // adding the collection
                    SetStatus = tempstatus,
                    FullInput = LabelDecrypt.Content.ToString(),
                    FullOutput = decodedoutput,
                    Input = System.IO.Path.GetFileName(LabelDecrypt.Content.ToString()),
                    Output = System.IO.Path.GetFileName(decodedoutput)
                });
                LabelDecrypt.Content = "No Input Selected";//reseting controls
                ButtonDecrypt.IsEnabled = false;
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);
            }
        }
    }

}
